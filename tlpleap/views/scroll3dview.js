/*
* Copyright (c) 2014, Josue Candela Perdomo
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This source is subject to the MIT License.
* Please see the http://opensource.org/licenses/MIT for more information.
* All other rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
*
* @autor Josué Candela Perdomo
* @email info@josuecp.es
* @date 2014-04-25
* @brief Class Scroll3DView that creates an interactive scrollable area that can be filled
*		 with Interactive3DViews
*/

var SCROLL3DVIEW_LINE_SIZE = {w: 50, h: 50};
var SCROLL3DVIEW_MARGIN_SIZE = 30;

var ScrollOrientation = {
	VERTICAL : 1,
	HORIZONTAL : 2
};

function Scroll3DView(obj){

	// Initialization
	this.scrolling = false;
	this.scrollX = 0;
	this.prevScrollX = 0;
	this.scrollZ = 0;
	this.prevScrollZ = 0;
	this.secondaryInteraction = SecondaryInteraction.SCROLL;
	this.lines = 3;
	this.orientation = ScrollOrientation.VERTICAL;
	this.views = [];
	this.position = {x:0, y:0};
	this.spacing = SCROLL3DVIEW_MARGIN_SIZE;
	this.lineSize = SCROLL3DVIEW_LINE_SIZE;
	
	// Object definition by params
	if (obj != null){
		if (obj.lines != null) this.lines = obj.lines;
		if (obj.orientation != null) this.orientation = obj.orientation;
		if (obj.position != null) this.position = obj.position;
		if (obj.spacing != null) this.spacing = obj.spacing;
		if (obj.lineSize != null) this.lineSize = obj.lineSize;
	}
	
	if (this.orientation == ScrollOrientation.HORIZONTAL){
		this.size = this.lineSize.h * this.lines +  this.spacing * (this.lines);
		var w = 400;
		var h = this.size;
	}else{
		this.size = this.lineSize.w * this.lines +  this.spacing * (this.lines);
		var w = this.size;
		var h = 400;
	}
	
	/*
	var geometry = new THREE.BoxGeometry(w, 0.1, h);
	var material = new THREE.MeshBasicMaterial({
			color : 0x55B3F2,
			wireframe : false,
			opacity : 0,
			transparent : true
		});
	this.mesh = new THREE.Mesh(geometry, material);
	*/
	
	this.mesh = new THREE.Object3D();

	this.mesh.position.x = this.position.x;
	this.mesh.position.y = -1;
	this.mesh.position.z = this.position.y;
	
	/**
	* This function updates the view and determines if the view or one of its
	* children is focused 
	*/
	this.update = function() {
		
		//Scrolling stop
		if (TLP3DGui.interactionType != TLP3DGui.INT_TYPE_1 && this.scrolling){
			this.scrolling = false;
		}
		
		
		this.focused = false;
		var pointerPos = TLP3DGui.areaGui.pointer.position;
		if (this.orientation == ScrollOrientation.VERTICAL){
			
			if ( pointerPos.x > this.mesh.position.x - this.size / 2 && 
				 pointerPos.x < this.mesh.position.x + this.size / 2)
				 this.focused = true;
		}else{
			if ( pointerPos.z > this.mesh.position.z - this.size / 2 && 
				 pointerPos.z < this.mesh.position.z + this.size / 2)
				 this.focused = true;
		}
		
		if (!this.scrolling){
			this.focusedChild = null;
			for (var i = 0; i < this.views.length; i++){
				this.views[i].update();
				if (this.views[i].focused)
					this.focusedChild = this.views[i];
			}
		}
	}
	
	/**
	* This function executes the primary action of the focused child if exists
	*/
	this.doPrimaryAction = function(){
		if (this.focusedChild != null){
			this.focusedChild.doPrimaryAction();
		}
	}
	
	/**
	* This function adds a view to the scrollview and repositions all the attached views
	*/
	this.addView = function(v) {
		this.views.push(v);
		
		//TODO - Reestructurar posición de los elementos
		this.mesh.add(v.mesh);
		this.updateViewPositions();
	}
	
	/**
	* This function repositions all the attached views
	*/
	this.updateViewPositions = function() {
		var i = 0;
		var j = 0;
		for (var index = 0; index < this.views.length; index++){
			var v  = this.views[index].mesh;
			v.position.x = ((i * this.lineSize.w) + ((i+1) * this.spacing) - this.size / 2) + this.scrollX + this.prevScrollX;
			v.position.z = ((j * this.lineSize.h) + ((j+1) * this.spacing) - this.size / 2) + this.scrollZ + this.prevScrollZ;
			//console.log(v.position + "  --> " + i + ", " + j);
			if (this.orientation == ScrollOrientation.VERTICAL){
				i++;
				if (i == this.lines){
					i = 0;
					j++;
				}
			}else{
				j++;
				if (j == this.lines){
					i++;
					j = 0;
				}
			}			
		}
	}
	
	/**
	* This function is called on scroll initialization and saves the previous state of the
	* scrolling
	*/
	this.initScroll = function(){

		this.prevScrollX += this.scrollX;
		this.prevScrollZ += this.scrollZ; 

		if (this.orientation == ScrollOrientation.VERTICAL)
			this.scrollInit = TLP3DGui.areaGui.pointer.position.z;
		else
			this.scrollInit = TLP3DGui.areaGui.pointer.position.x;
			
		this.scrolling = true;
	}
	
	/**
	* This function performs the scrolling
	*/
	this.scroll = function(){
	
		var current = 0;
		if (this.orientation == ScrollOrientation.VERTICAL){
			current = TLP3DGui.areaGui.pointer.position.z;
			this.scrollZ =  current - this.scrollInit;
		}else{
			current = TLP3DGui.areaGui.pointer.position.x;
			this.scrollX = current -this.scrollInit;
		}
		this.updateViewPositions();
			
		
			
	}
	
	/**
	* This function removes all the views of the scrollview
	*/
	this.clearViews = function(){
		var view = this.mesh;
		var n_child = view.children.length;
		for (i = 0; i < n_child; i++) {
			var child = view.children[((n_child - 1) -i)];
			view.remove(view.children[((n_child - 1) -i)]);
		}
		this.views = [];
	}

};