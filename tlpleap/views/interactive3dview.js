/*
* Copyright (c) 2014, Josue Candela Perdomo
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This source is subject to the MIT License.
* Please see the http://opensource.org/licenses/MIT for more information.
* All other rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
*
* @autor Josué Candela Perdomo
* @email info@josuecp.es
* @date 2014-04-14
* @brief Contains Class Interactive3DView that creates an interactive item with contextual 
*		 actions that can be attached to a scene.
*		 Contains Class ContextualAction that defines a contextual action to be added to
*		 an Interactive3DView and showed on the 'Contextual Gui'
*/

var INT3DVIEW_SIZE = 30; //

function Interactive3DView(def){
	
	this.init = function(def){
		// Initialization
		this.secondaryInteraction = SecondaryInteraction.MENU;
		this.focused = false;	
		this.actions = [];
		this.position = {x: 0, y:0};
		this.size = INT3DVIEW_SIZE;
		this.icon = null;
	
		// Object definition by params
		if (def != null){
			if (def.actions != null)  this.actions = def.actions;
			if (def.position != null) this.position = def.position;
			if (def.size != null) this.size = def.size;
			this.icon = def.icon || null;
		}

		var geometry = new THREE.BoxGeometry(this.size, this.size, this.size);
		var material = new THREE.MeshBasicMaterial({
				color : 0xffffff,
				wireframe : false,
				opacity : 1,
				transparent : true,
				map : (this.icon != null? THREE.ImageUtils.loadTexture(this.icon) : null)
			});
		this.mesh = new THREE.Mesh(geometry, material);

		this.mesh.position.x = this.position.x;
		this.mesh.position.z = this.position.y;

	}

	/**
	* This function updates the view and determines if the is focused
	*/
	this.update = function(){
		var posPointer = [TLP3DGui.areaGui.pointer.position.x, TLP3DGui.areaGui.pointer.position.z];
		var posMesh = [this.mesh.position.x, this.mesh.position.z];
		if (MathUtils.distance(posPointer, posMesh) < this.size / 1.5){
			if (!this.focused){
				this.focus();
			}
		}else{
			this.mesh.position.y = 0;
			this.focused = false;
		}
		
		
	}
	
	/**
	* This function focuses the view showing an animation
	*/
	this.focus = function(){
		var _inst = this;
		var pos = this.mesh.position;
		var target = { y : 30 };
		var tween = new TWEEN.Tween(pos).to(target, 200);
		tween.onUpdate(function () {
			_inst.mesh.position.y = pos.y;
		});
		tween.easing(TWEEN.Easing.Quartic.Out);
		tween.start();
		
		this.focused = true;
		
		//TODO - Mostrar nombre elemento o acción, e.g: Abrir 'Apps', donde [Nombre de accion][Nombre de elemento]
	}
	
	/**
	* This function adds and action to the view that will be shown on the 'Contextual Gui'
	*/
	this.addAction = function(a){
		this.actions.push(a);
	}
	
	/**
	* This function directly executes the contextual action marked as isPrimary = true
	*/
	this.doPrimaryAction = function(){
		for (var i = 0; i < this.actions.length; i++){
			//console.log(this.actions[i]);
			if (this.actions[i].isPrimary){
				console.log("[TLP3DGui][info] Found primary action, executing...");
				this.actions[i].run();
			}
		}
				
	}
	
	this.init(def);
}

function ContextualAction(icon, label, onRun, primary){
	this.icon = icon;
	this.label = label;
	this.run = onRun;
	this.isPrimary = primary;
}
