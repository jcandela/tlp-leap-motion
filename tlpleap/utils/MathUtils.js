/*
* Copyright (c) 2014, Universidad de La Laguna
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This source is subject to the MIT License.
* Please see the http://opensource.org/licenses/MIT for more information.
* All other rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
*
* @autor Josué Candela Perdomo
* @email info@josuecp.es
* @date 2014-03-31
* @brief Contains variuos functions related with mathematical operations
*/

var MathUtils = {};

/**
* Returns the normalized vector of the vector given
* @param v Vector to be normalized
* @return Normalized vector (Array of three elements)
*/
MathUtils.vectorNormalize = function(v){
	var n = [0,0,0];
	var a = Math.sqrt((v[0] * v[0]) + (v[1] * v[1]) + (v[2] * v[2]));
	n[0] = v[0] / a;
	n[1] = v[1] / a;
	n[2] = v[2] / a;
	
	return n;
}

MathUtils.distance = function(v1, v2){
	return Math.sqrt(Math.pow(v1[0] - v2[0], 2) + Math.pow(v1[1] - v2[1], 2));
}