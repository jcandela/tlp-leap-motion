/*
* Copyright (c) 2014, Josue Candela Perdomo
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This source is subject to the MIT License.
* Please see the http://opensource.org/licenses/MIT for more information.
* All other rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
*
* @autor Josué Candela Perdomo
* @email info@josuecp.es
* @date 2014-04-25
* @brief Simple Class TLP3DScene that contains all the views that will be attached to the 
*		 root view
*/

function TLP3DScene(id, obj) {

	this.id = id;
	this.views = [];

	if (obj != null) {
		this.views = obj.views;
	}

	/**
	* This function adds a view to the scene, it will be created on scene selection
	*/
	this.addView = function(v) {
		this.views.push(v);
	}
};