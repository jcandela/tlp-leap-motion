/*
* Copyright (c) 2014, Josue Candela Perdomo
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This source is subject to the MIT License.
* Please see the http://opensource.org/licenses/MIT for more information.
* All other rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
*
* @autor Josué Candela Perdomo
* @email info@josuecp.es
* @date 2014-03-31
* @brief Contains the TLP3DGui API that manages to create adapted interfaces for
* 		 LEAP Motion device.
*/

var TLP3DGui = {};

TLP3DGui.DEBUG = true;

TLP3DGui.INT_TYPE_NONE = "_INT_TYPE_NONE"; 	// Not interaction has been defined
TLP3DGui.INT_TYPE_3D   = "_INT_TYPE_3D"; 	// Indicates that the user is interacting with the 3D elements

TLP3DGui.INT_TYPE_1    = "_INT_TYPE_1"; 	// Indicates that the user is as an primary action
TLP3DGui.INT_TYPE_2    = "_INT_TYPE_2"; 	// Indicates that the user is as an secondary action

TLP3DGui.CONTEXTUAL_GUI_ACTION_SIZE = 25; 	// Indicates the icons size of one action of the contextual gui

TLP3DGui.TOUCH_DIST = 15;					// Indicates the focus distance of an interactive view

TLP3DGui.grabCaptured = false;				// Indicates if the current grab action has been captured
TLP3DGui.closeContextual;

// API elements
TLP3DGui.sceneManager = {};
TLP3DGui.areaGui = {};
TLP3DGui.contextualGui = {};
TLP3DGui.audio = {};


TLP3DGui.audio.click = new Audio('res/click.wav');
TLP3DGui.audio.sceneTransition = new Audio('res/whoosh.wav');

/**
* This function initializes the engine and creates the main view where the scenes will
* be created
*/
TLP3DGui.init = function () {

	TLP3DGui.renderer = new THREE.WebGLRenderer();
	TLP3DGui.renderer.setSize(window.innerWidth, window.innerHeight);
	TLP3DGui.renderer.setClearColor( 0xffffff, 1 );
	document.body.appendChild(TLP3DGui.renderer.domElement);

	TLP3DGui.interactionType = TLP3DGui.INT_TYPE_NONE;
	TLP3DGui.interactionAllowed = true;
	
	THREE.ImageUtils.crossOrigin = "anonymous";

	// CAMERA
	var cam1 = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 1000);
	cam1.position.z = 300;
	cam1.position.y = 200;
	cam1.rotation.x = -30 * Math.PI / 180;

	TLP3DGui.camera = cam1;

	// BASE SCENE
	TLP3DGui.scene = new THREE.Scene();

	// CONTEXT GUI LAYER
	TLP3DGui.contextualGui.create();

	// AREA GUI LAYER
	TLP3DGui.areaGui.create();
	
	// HAND POINTERS
	TLP3DGui.createPointers();

	// BASIC LIGHT
	TLP3DGui.scene.add(new THREE.AmbientLight(0xffffff));

	TLP3DGui.update();

	window.addEventListener('resize', TLP3DGui.onWindowResize, false);

	if (TLP3DGui.DEBUG) {
		// DEBUG CAMERA
		//var cam2 = new THREE.OrthographicCamera(window.innerWidth /  - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight /  - 2, 1, 1000);
		//cam2.position.z = 0;
		//cam2.position.y = 200;
		//cam2.rotation.x = -90 * Math.PI / 180;

		var cam2 = new THREE.PerspectiveCamera(70, window.innerWidth / window.innerHeight, 1, 1000);
		cam2.position.z = 0;
		cam2.position.y = 300;
		cam2.rotation.x = -90 * Math.PI / 180;

		// DEBUG KEYS FOR CAMERA CHANGE
		window.onkeyup = function (e) { // Camera change listener
			var key = e.keyCode ? e.keyCode : e.which;

			if (key == 38) {
				TLP3DGui.camera = cam1;
			} else if (key == 40) {
				TLP3DGui.camera = cam2;
			}
			
		}

		
	}

	// GESTURE RECOGNITION
	TLPLeap.gestures.addListener(new GesturePushHolding(), function (data) {
		//console.log(JSON.stringify(data));
		if (data.detected) {
			if (TLP3DGui.interactionType == TLP3DGui.INT_TYPE_2) {
				TLP3DGui.contextualGui.touch();
			}
		} else {
			if (TLP3DGui.interactionType == TLP3DGui.INT_TYPE_2) {
				TLP3DGui.contextualGui.updateTouch(data.time / GesturePushHolding.holdInterval);
			}
		}
	});
}

/**
* This function detects what kind of interaction is in progress and updates all the 
* views in consequence
*/
TLP3DGui.update = function () {

	requestAnimationFrame(TLP3DGui.update);

	TLP3DGui.updatePointers();

	TLP3DGui.checkUserInteraction();

	TLP3DGui.renderer.render(TLP3DGui.scene, TLP3DGui.camera);
	

	TWEEN.update();
}

/**
* This function determines how to react to the user interaction
*/
TLP3DGui.checkUserInteraction = function(){
	// User interaction
	if (TLP3DGui.interactionAllowed) {
		if (TLP3DGui.interactionType == TLP3DGui.INT_TYPE_1) {
			if (TLP3DGui.contextualGui.isVisible)
				return;
			
			var item = TLP3DGui.areaGui.getFocused();			
			if (item != null){
			
				switch(item.secondaryInteraction){
				case SecondaryInteraction.MENU:
					if (!TLP3DGui.grabCaptured){
						console.log("[TLP3DGui][info] Executing primary action on focused view");
						item.doPrimaryAction();
						TLP3DGui.audio.click.play();
						TLP3DGui.grabCaptured = true;
					}
					break;
				case SecondaryInteraction.SCROLL:
					if (!item.scrolling)
						item.initScroll();
					console.log("[TLP3DGui][info] Scrolling ");
					item.scroll();
					TLP3DGui.areaGui.update();					
					break;
				}
			
			}else{
				console.log("[TLP3DGui][info] Trying to execute primary action but none view selected");
			}
			
		
		}else if (TLP3DGui.interactionType == TLP3DGui.INT_TYPE_2) {
				clearTimeout(TLP3DGui.closeContextual);
				TLP3DGui.closeContextual = null;
				
				if (TLP3DGui.contextualGui.isVisible){
					TLP3DGui.contextualGui.update();
					return;
				}

				var item = TLP3DGui.areaGui.getFocused();
				if (item != null){
			
					switch(item.secondaryInteraction){
					case SecondaryInteraction.MENU:

						if (item.actions.length == 0)
							return;	 //Prevents to show the contextual gui without actions
				
						if (item.actions.length == 1){
							TLP3DGui.audio.click.play();	
							item.doPrimaryAction();	// Executes the only available action 
							return;
						}
						
						// Show the contextual gui to allow the user choose the action
						if (!TLP3DGui.contextualGui.isVisible){
							console.log("[TLP3DGui][info] Executing secondary interaction: " + item.actions.length + ", secondarytype: " + item.secondaryInteraction);
							TLP3DGui.contextualGui.show();
						}

						break;
					case SecondaryInteraction.SCROLL:	
						break;
					}
				}
			

		} else {
			
			if (TLP3DGui.contextualGui.isVisible){
				if (TLP3DGui.closeContextual == null)
					TLP3DGui.closeContextual = setTimeout(TLP3DGui.contextualGui.hide, 300);
			}
			
			TLP3DGui.areaGui.update();
		}
		

	}
}

/**
* This function creates the hand dummy and finger pointers and attaches them to the scene
*/
TLP3DGui.createPointers = function () {
	TLP3DGui.pointers = [];
	for (var i = 0; i < 10; i++) {
		var geometry = new THREE.BoxGeometry(10, 10, 10);
		var material = new THREE.MeshBasicMaterial({
				color : 0x333333,
				wireframe : true,
				opacity : 0.1,
				transparent : true
			});
		var mesh = new THREE.Mesh(geometry, material);
		mesh.visible = false;

		TLP3DGui.scene.add(mesh);
		TLP3DGui.pointers[i] = mesh;
	}

	var geometry = new THREE.CylinderGeometry(25, 25, 20, 32);
	var material = new THREE.MeshBasicMaterial({
			color : 0x333333,
			wireframe : true,
			opacity : 0.1,
			transparent : true
		});
	var mesh = new THREE.Mesh(geometry, material);
	TLP3DGui.scene.add(mesh);
	TLP3DGui.pointers[i] = mesh;

	TLP3DGui.pointers.main = mesh;

}

/**
* This function determines if at least one finger data is available
*/
TLP3DGui.existPointer = function () {
	if (TLPLeap.data == null)
		return false;

	
	var hand = TLPLeap.data.hands[0];
	if (hand == null)
		return false;

	var pointables = hand.pointables;
	if (pointables == null)
		return false;

	return true;
}

/**
* This function updates the hand dummy and finger pointers
*/
TLP3DGui.updatePointers = function () {
	if (!TLP3DGui.existPointer())
		return;

	var hand = TLPLeap.data.hands[0];
	var pointables = hand.pointables;

	for (var i = 0; i < TLP3DGui.pointers.length; i++)
		TLP3DGui.pointers[i].visible = false;
	// This determines which kind of interaction is

	if (pointables.length <= 1)
		TLP3DGui.interactionType = TLP3DGui.INT_TYPE_1;
	else if (pointables.length == 2)
		TLP3DGui.interactionType = TLP3DGui.INT_TYPE_2;
	else
		TLP3DGui.interactionType = TLP3DGui.INT_TYPE_3D;

	if (pointables.length > 1 && TLP3DGui.grabCaptured)
		TLP3DGui.grabCaptured = false;

	var main_pos = [];
	for (var i = 0; i < pointables.length; i++) {
		var p = pointables[i];
		var pos = p.stabilizedTipPosition;
		TLP3DGui.pointers[i].visible = true;
		TLP3DGui.pointers[i].position.x = pos[0];
		TLP3DGui.pointers[i].position.y = pos[1];
		TLP3DGui.pointers[i].position.z = pos[2];
	}

	TLP3DGui.pointers.main.visible = true;

	TLP3DGui.pointers.main.position.x = hand.palmPosition[0];
	TLP3DGui.pointers.main.position.y = hand.palmPosition[1];
	TLP3DGui.pointers.main.position.z = hand.palmPosition[2];

}

/**
* This function listens to the window resize to resize the scene accordingly 
*/
TLP3DGui.onWindowResize = function () {
	TLP3DGui.camera.aspect = window.innerWidth / window.innerHeight;
	TLP3DGui.camera.updateProjectionMatrix();

	TLP3DGui.renderer.setSize(window.innerWidth, window.innerHeight);
}

// ----------------------------------------------------------------------------------------
// Area GUI ------------------------------------------------------------------------------- [xAre]
// ----------------------------------------------------------------------------------------

TLP3DGui.AREA_GUI_SIZE = 400;	// Determines the size of the interaction area
TLP3DGui.areaGui.views = [];	// Contains all the views attached to the root view

TLP3DGui.areaGui.interactivityAllowed = true;
/**
* This function creates the 'Area GUI' and attaches a pointer to it
*/
TLP3DGui.areaGui.create = function () {
	var width = window.innerWidth / 3;
	var height = window.innerHeight / 3;

	TLP3DGui.areaGui.mesh = new THREE.Object3D();

	if (TLP3DGui.DEBUG){
		var helper = new THREE.GridHelper(TLP3DGui.AREA_GUI_SIZE / 2, 50);
		helper.setColors(0x0000ff, 0x808080);
		helper.position.y = 0;
		TLP3DGui.scene.add(helper);
	}
		
	// 3D context limits
	var geometry = new THREE.BoxGeometry(400, 1, 400);
	var material = new THREE.MeshBasicMaterial({
			color : 0xffffff,
			wireframe : false,
			opacity : 1,
			map: THREE.ImageUtils.loadTexture("res/wallpaper.jpg")
		});
		
	var f = new THREE.Mesh(geometry, material);
	f.position.y = -30;
	
	
	// 3D context pointer
	var geometry = new THREE.SphereGeometry(10, 10, 10);
	var material = new THREE.MeshBasicMaterial({
			color : 0x3355ff,
			wireframe : false,
			opacity : 0.5,
			transparent : true
		});
	var pointer = new THREE.Mesh(geometry, material);

	TLP3DGui.areaGui.pointer = pointer;

	TLP3DGui.areaGui.mesh.add(pointer);
    
    TLP3DGui.scene.add(f);
	TLP3DGui.scene.add(TLP3DGui.areaGui.mesh);
}

/**
* This function updates the 'Area Gui', updates the pointer position and the views
* attached to the root view
*/
TLP3DGui.areaGui.update = function(){
	if (TLPLeap.data == null)
		return;
	var hand = TLPLeap.data.hands[0];
	if (hand == null)
		return;
	

	var px = (hand.palmPosition[0] / TLP3DGui.AREA_GUI_SIZE * 2);
	var pz = (hand.palmPosition[2] / TLP3DGui.AREA_GUI_SIZE * 2);

	var x = px * TLP3DGui.AREA_GUI_SIZE;
	var z = pz * TLP3DGui.AREA_GUI_SIZE;
	
	TLP3DGui.areaGui.pointer.position.x = x;
	TLP3DGui.areaGui.pointer.position.z = z;
	
	//This updates all the current GUI elements
	for (var i = 0; i < TLP3DGui.areaGui.views.length; i++){
		TLP3DGui.areaGui.views[i].update();
	}
}

/**
* This function adds a view to the root view and shows it on the 'Area Gui'
* @param v View to be added
*/
TLP3DGui.areaGui.addView = function(v){
	TLP3DGui.areaGui.mesh.add(v.mesh);
	this.views.push(v);
	v.update();
	//console.log("añadida vista al areagui");
}

/**
* This function returns wich view is being focused by the 'Area Gui' pointer
*/
TLP3DGui.areaGui.getFocused = function(){
	for (var v  = 0; v < TLP3DGui.areaGui.views.length; v++){
		var view = TLP3DGui.areaGui.views[v];
		if (view.focused){
			if (view.focusedChild != null)
				return view.focusedChild;
			else
				return view;
		}
	}
}

TLP3DGui.areaGui.clear = function(){
	var view = TLP3DGui.areaGui.mesh;
	var n_child = view.children.length;
	for (i = 0; i < n_child; i++) {
		var child = view.children[((n_child - 1) -i)];
		if (child != TLP3DGui.areaGui.pointer)
			view.remove(view.children[((n_child - 1) -i)]);
	}
	TLP3DGui.areaGui.views = [];
}

// ----------------------------------------------------------------------------------------
// Scene Manager -------------------------------------------------------------------------- [xScn]
// ----------------------------------------------------------------------------------------

TLP3DGui.sceneManager.scenes = []; // Contains all the created scenes

/**
* This function adds a scene to the scene list, indexed by its id
* @param scn Scene to be added
*/
TLP3DGui.sceneManager.add = function (scn) {
		
	TLP3DGui.sceneManager.scenes[scn.id] = scn;
	console.log("[TLP3D][info] : Added scene: " + scn.id);
}

/**
* This function shows the given scene
* @param id Id of the selected scene
* @param anim Boolean that indicates if animation should be performed
*/
TLP3DGui.sceneManager.select = function (id, anim) {
	var scn = TLP3DGui.sceneManager.scenes[id];
	if (scn == null)
		alert("Scene not found");
		
	if (anim || anim == null){
		TLP3DGui.sceneManager.startTransition(function () { //onEnd transition
			TLP3DGui.sceneManager.clear()
			TLP3DGui.sceneManager.create(scn);
			TLP3DGui.sceneManager.endTransition();
			TLP3DGui.audio.sceneTransition.play();
		});
	}else{
		TLP3DGui.sceneManager.clear()
		TLP3DGui.sceneManager.create(scn);
	}
}

/**
* This function starts the scene change transition and cancels the interactivity
* @param onEnd Function that is going to be executed on animation end
*/
TLP3DGui.sceneManager.startTransition = function (onEnd) {
	var rot = TLP3DGui.contextualGui.mesh.rotation;
	rot.y = 0;
	var target = {
		y : Math.PI
	};
	var tween = new TWEEN.Tween(rot).to(target, 500);
	tween.onUpdate(function () {
		TLP3DGui.camera.rotation.y = rot.y;
	});
	tween.onComplete(onEnd);
	tween.easing(TWEEN.Easing.Quartic.In);
	tween.start();
	
	TLP3DGui.interactionAllowed = false;
	
	TLP3DGui.contextualGui.hide();
}

/**
* This function shows the rest of the transition animation and restores the interactivity
*/
TLP3DGui.sceneManager.endTransition = function () {
	var rot = TLP3DGui.contextualGui.mesh.rotation;
	var target = {
		y : Math.PI * 2
	};
	var tween = new TWEEN.Tween(rot).to(target, 500);
	tween.onUpdate(function () {
		TLP3DGui.camera.rotation.y = rot.y;
	});
	//tween.onComplete(onEnd);
	tween.easing(TWEEN.Easing.Quartic.Out);
	tween.start();
	
	TLP3DGui.interactionAllowed = true;
}

/**
* This function removes all the views of the root view
*/
TLP3DGui.sceneManager.clear = function () {
	TLP3DGui.areaGui.clear();
}

/**
* This function creates the gives scene on the 'Area Gui'
* @param scn Scene to be created
*/
TLP3DGui.sceneManager.create = function (scn) {
	if (scn.views == null)
		return;

	for (var i = 0 ; i < scn.views.length; i++){
		TLP3DGui.areaGui.addView(scn.views[i]);
	}
}

// ----------------------------------------------------------------------------------------
// Contextual GUI ------------------------------------------------------------------------- [xCtx]
// ----------------------------------------------------------------------------------------

/**
* This function creates the contextual gui root view and attaches a ponter to it
*/
TLP3DGui.contextualGui.create = function () {
	var width = window.innerWidth / 3;
	var height = window.innerHeight / 3;

	// 2D context limits
	var geometry = new THREE.BoxGeometry(width, height, 10);
	var material = new THREE.MeshBasicMaterial({
			color : 0x000000,
			wireframe : false,
			opacity : 0.5,
			transparent : true,
		});
	var mesh = new THREE.Mesh(geometry, material);
	mesh.position.z = 200;
	mesh.position.y = 100;
	mesh.rotation.x = -40 * Math.PI / 180;

	TLP3DGui.contextualGui.mesh = mesh;
	TLP3DGui.scene.add(TLP3DGui.contextualGui.mesh);

	// 2D context pointer
	var geometry = new THREE.SphereGeometry(10, 10, 10);
	var material = new THREE.MeshBasicMaterial({
			color : 0x3355ff,
			wireframe : false,
			opacity : 0.5,
			transparent : true
		});
	var pointer = new THREE.Mesh(geometry, material);
	pointer.position.z = 50;
	TLP3DGui.contextualGui.pointer = pointer;
	TLP3DGui.contextualGui.width = width;
	TLP3DGui.contextualGui.height = height;
	TLP3DGui.contextualGui.mesh.add(pointer);

	TLP3DGui.contextualGui.actions = new THREE.Object3D();

	//TLP3DGui.contextualGui.actions.position.y = - TLP3DGui.contextualGui.height / 2;

	TLP3DGui.contextualGui.mesh.add(TLP3DGui.contextualGui.actions);

	TLP3DGui.contextualGui.hide();
}

/**
* This function toggles the visibility of the 'Contextual Gui'
*/
TLP3DGui.contextualGui.toggleVisibility = function () {
	if (TLP3DGui.contextualGui.isVisible) {
		TLP3DGui.contextualGui.hide();
	} else {
		TLP3DGui.contextualGui.show();
	}
}

/**
* This function builds and shows the 'Contextual Gui' making an animation
*/
TLP3DGui.contextualGui.show = function () {
	TLP3DGui.areaGui.interactivityAllowed = false;

	TLP3DGui.contextualGui.clear();
	var item = TLP3DGui.areaGui.getFocused()
	console.log("SHOWING CONTEXTUAL: " + item);
	if (item != null){
		//console.log("Mostrando menu contextual");
		//console.log("-------------------------");
		for (var i = 0; i < item.actions.length; i++){
			var action = item.actions[i];
			//console.log("Accion: " + action.label);
			TLP3DGui.contextualGui.addAction(action);
		}
	}else{
		console.log("No hay elemento seleccionado");
	}
	
	var pos = TLP3DGui.contextualGui.mesh.position;
	var target = {
		z : 150
	};
	var tween = new TWEEN.Tween(pos).to(target, 300);
	tween.onUpdate(function () {
		TLP3DGui.contextualGui.mesh.position.z = pos.z;
	});

	TLP3DGui.contextualGui.mesh.traverse(function (object) {
		object.visible = true;
	});

	tween.easing(TWEEN.Easing.Quartic.In);
	tween.start();

	TLP3DGui.contextualGui.isVisible = true;
	
	
	
}

/**
* This function hides the 'Contextual Gui' making an animation
*/
TLP3DGui.contextualGui.hide = function () {
	console.log("hidding CONTEXTUAL: ");

	TLP3DGui.areaGui.interactivityAllowed = true;
	
	//console.log("contextualGui.hide");
	var pos = TLP3DGui.contextualGui.mesh.position;
	var target = {
		z : 500
	};
	var tween = new TWEEN.Tween(pos).to(target, 300);
	tween.onUpdate(function () {
		TLP3DGui.contextualGui.mesh.position.z = pos.z;
	});
	tween.onComplete(function () {
		TLP3DGui.contextualGui.mesh.traverse(function (object) {
			object.visible = false;
		});
	});
	tween.easing(TWEEN.Easing.Quartic.Out);
	tween.start();

	TLP3DGui.contextualGui.isVisible = false;
}

/**
* This function updates the touch gesture and sets the pointer opacity accordingly
* @param level Current level of touch (between 1 and 0)
*/
TLP3DGui.contextualGui.updateTouch = function (level) {
	if (level < 0.5)
		return;

	var p = TLP3DGui.contextualGui.pointer;
	p.material.opacity = level;
}

/**
* This function performs the touch interaction and  executes the action selected if exist
*/
TLP3DGui.contextualGui.touch = function () {
	var touch_dist = 0;

	var pointer_pos = TLP3DGui.contextualGui.pointer.position;
	var actions = TLP3DGui.contextualGui.actions;
	var n_child = actions.children.length;

	var min_dist = 0;
	var min_dist_index = -1;
	for (var i = 0; i < n_child; i++) {
		var c = actions.children[i];
		var dist = MathUtils.distance([pointer_pos.x, pointer_pos.y], [c.position.x, c.position.y])
			if (min_dist_index == -1 || dist < min_dist) {
				min_dist = dist;
				min_dist_index = i;
			}
	}
	console.log("[TLP3DGui][info] : Performing action ... " + min_dist_index + ", " + min_dist);

	if (min_dist_index != -1 && min_dist < TLP3DGui.TOUCH_DIST){
		TLP3DGui.contextualGui.doAction(min_dist_index);
		TLP3DGui.contextualGui.hide();
	}
}

/**
* This function executes the selected (touched) action of the 'Contextual Gui'
* @param index Index of the selected action
*/
TLP3DGui.contextualGui.doAction = function (index) {
	console.log("contextualGui.doAction: Performing action ID: " + index);
	TLP3DGui.contextualGui.actions.children[index].run();
}

/**
* This function adds an action to the 'Contextual Gui'
* @param act ContextualAction object to add to the view
*/
TLP3DGui.contextualGui.addAction = function (act){
		
	//Action icon
	var actionSize = TLP3DGui.CONTEXTUAL_GUI_ACTION_SIZE;
	var geometry = new THREE.PlaneGeometry(actionSize, actionSize);
	//var material = new THREE.MeshLambertMaterial({ map: THREE.ImageUtils.loadTexture(icon)});
	var material = new THREE.MeshBasicMaterial({
			color: 0xffffff,
			transparent : true,
			map: (act.icon != null? THREE.ImageUtils.loadTexture(act.icon) : null)
		});
	var actionMesh = new THREE.Mesh(geometry, material);

	// Action label
	var txt = makeTextSprite(act.label, {
			fontsize : 14,
			color: "#ffffff"
		});
	
	txt.position.y = -20;	
	
	actionMesh.add(txt);

	// Additional fields
	actionMesh.label = act.label;
	actionMesh.run = act.run;
	actionMesh.position.z = 50;

	TLP3DGui.contextualGui.actions.add(actionMesh);
	//TLP3DGui.contextualGui.mesh.add( actionMesh );
	TLP3DGui.contextualGui.refresh();
}

/**
 * This function updates the position of the contextual gui actions
 * in a radial shape
 */
TLP3DGui.contextualGui.refresh = function () {
	var actions = TLP3DGui.contextualGui.actions;
	var radius = 50;
	var n_child = actions.children.length;

	//console.log("contextualGui.refresh : " + n_child + " children");
	if (n_child == 1) {
		actions.children[0].position.x = 0;
		actions.children[0].position.y = 0;
		return;
	}

	//TODO - improve initial angle for 3 actions

	var angle_inc = 360 / n_child;

	for (var i = 0; i < n_child; i++) {
		var angle = (angle_inc * i) * Math.PI / 180;
		actions.children[i].position.x = Math.cos(angle) * radius;
		actions.children[i].position.y = Math.sin(angle) * radius;
		//console.log(angle);
		//angle += angle_inc;
	}
}

/**
* This function removes all the actions of the 'Contextual Gui' root view
*/
TLP3DGui.contextualGui.clear = function(){
	var actions = TLP3DGui.contextualGui.actions;
	var n_child = actions.children.length;
	for (i = 0; i < n_child; i++) {
		TLP3DGui.contextualGui.actions.remove(TLP3DGui.contextualGui.actions.children[((n_child - 1) -i)]);
	}
}

/**
* This function updates the 'Contextual Gui' pointer position
*/
TLP3DGui.contextualGui.update = function () {
	if (!TLP3DGui.existPointer())
		return;

	var pointer = TLPLeap.data.hands[0].pointables[0];

	var px = (pointer.stabilizedTipPosition[0] / TLPLeap.data.interactionBox.width) / 2;
	var py = (pointer.stabilizedTipPosition[1] / TLPLeap.data.interactionBox.height) - 0.5;

	var x = px * TLP3DGui.contextualGui.width;
	var y = py * TLP3DGui.contextualGui.height;

	TLP3DGui.contextualGui.pointer.position.x = x;
	TLP3DGui.contextualGui.pointer.position.y = y;

}

// ----------------------------------------------------------------------------------------
// Enums ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------

var SecondaryInteraction = {
	MENU : 0,
	SCROLL : 1,
}

// ----------------------------------------------------------------------------------------
// Utils ----------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------

/**
* This function creates a 3D sprite containing the given text
* @param message String text to be transformed in 3D
* @param parameters Additional object with the following parameters
* 		- fontface
*		- fontsize
*		- textColor
*		- textAlign
*/
function makeTextSprite(message, parameters) {
	if (parameters === undefined)
		parameters = {};

	var fontface = parameters.hasOwnProperty("fontface") ?
		parameters["fontface"] : "Arial";

	var fontsize = parameters.hasOwnProperty("fontsize") ?
		parameters["fontsize"] : 18;

	var canvas = document.createElement('canvas');
	var context = canvas.getContext('2d');
	context.font = "" + fontsize + "px " + fontface;

	// get size data (height depends only on font size)
	//var metrics = context.measureText( message );
	//var textWidth = metrics.width;

	// text color
	context.fillStyle = parameters.hasOwnProperty("color") ?
		parameters["color"] : "#ffffff";
	context.textAlign = parameters.hasOwnProperty("textAlign") ?
		parameters["textAlign"] : "center";
	context.fillText(message, 150, 70);

	// canvas contents will be used for a texture
	var texture = new THREE.Texture(canvas)
		texture.needsUpdate = true;

	var spriteMaterial = new THREE.SpriteMaterial({
			map : texture
		});
	var sprite = new THREE.Sprite(spriteMaterial);
	sprite.scale.set(100, 50, 1.0);
	return sprite;
}