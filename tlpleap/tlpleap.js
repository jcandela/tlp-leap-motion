
/*
 * Copyright (c) 2014, Universidad de La Laguna
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This source is subject to the MIT License.
 * Please see the http://opensource.org/licenses/MIT for more information.
 * All other rights reserved.
 *
 * THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
 * KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
 * PARTICULAR PURPOSE.
 *
 * @autor Josué Candela Perdomo
 * @email info@josuecp.es
 * @date 2014-03-31
 * @brief Contains TLPLeap class that manages all about LEAP motion gesture detection
 */
TLPLeap = {};

/**
 * This function initializes the gesture recognition
 */
TLPLeap.init = function () {

	Leap.loop(TLPLeap.update);

}

/**
 * This function updates all the modules
 */
TLPLeap.update = function (leap_obj) {

	//console.log(leap_obj);
	TLPLeap.data = leap_obj

		TLPLeap.gestures.update(leap_obj);

	if (TLPLeap.updateCallback != null)
		TLPLeap.updateCallback(leap_obj);

};

/**
 * Stores all the methods related with the gestures
 */
TLPLeap.gestures = {
	tracked : [],

	/**
	 * This function adds a listener to a given gesture detector
	 * @param gestureDetector which is going to be tracked
	 * @param cback, once the gesture is recognized, the callback is called
	 */
	addListener : function (gestureDetector, cback) {
		TLPLeap.gestures.tracked.push({
			detector : gestureDetector,
			callback : cback
		})
	},

	update : function (leap_obj) {
		//update all the tracked gestures
		for (var i = 0; i < TLPLeap.gestures.tracked.length; i++) {

			var gt = TLPLeap.gestures.tracked[i];
			var data = gt.detector.update(leap_obj);

			if (data != null || gt.detector.isDetected())
				gt.callback(data);

		}
	}
};
