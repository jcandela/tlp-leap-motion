
/*
* Copyright (c) 2014, Universidad de La Laguna
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This source is subject to the MIT License.
* Please see the http://opensource.org/licenses/MIT for more information.
* All other rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
*
* @autor Josué Candela Perdomo
* @email info@josuecp.es
* @date 2014-03-31
* @brief Contains the basic methods of a basic gesture that recognizes when two finger points are in horizontal
*/

function GestureTwoHorizontal (gap) {        
	this._gap = 5;
	this._detected = false;
	
	this.update  = function(leap_obj) {
		this._detected = false;
		
		var angle = 0;
		var prevPoint = null;
		var pointablesMap = leap_obj.pointablesMap;		
		for (var i in pointablesMap) {
			// get the pointable's position
			var pointable = pointablesMap[i];
			var pos = pointable.tipPosition; // Vector 3 with point position		
			//pos = MathUtils.vectorNormalize(pos);
			if (prevPoint != null)
			{								
				angle = (Math.atan2(pos[0] - prevPoint[0], pos[1] - prevPoint[1]) * 180 / Math.PI) + 90;
				//console.log(angle);
				if (Math.abs(angle) < this._gap || Math.abs(angle-180) < this._gap){					
					this._detected=true;
					return {angle: angle, x: pos[0], y:pos[1]};
				}
			}			
			prevPoint = pos;
		}
		
		return null;
	}
	
	this.isDetected = function(){
		return this._detected;
	}
}