
/*
* Copyright (c) 2014, Universidad de La Laguna
* DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
*
* This source is subject to the MIT License.
* Please see the http://opensource.org/licenses/MIT for more information.
* All other rights reserved.
*
* THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
* KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
* PARTICULAR PURPOSE.
*
* @autor Josué Candela Perdomo
* @email info@josuecp.es
* @date 2014-05-20
* @brief Contains the basic methods of a basic gesture that recognizes when a finger has been hold in the same position for a while (_holdInterval)
*/

function GesturePushHolding (gap) {        
	GesturePushHolding.holdInterval = 1000; // One second holding the pointer

	this._gap = 25;
	this._detected = false;
	this._lastpos = [];
	this._times = [];
	
	this.update  = function(leap_obj) {
		this._detected = false;
		
		var angle = 0;
		var prevPoint = null;
		var pointablesMap = leap_obj.pointablesMap;		
		for (var i in pointablesMap) {
			// get the pointable's position
			var pointable = pointablesMap[i];
			
			var pos = pointable.tipPosition; // Vector 3 with point position		

			
			if (this._lastpos[pointable.id] == null){
				this._lastpos[pointable.id] = pos;
				this._times[pointable.id] = 0;
			}else{
				var dist = MathUtils.distance(this._lastpos[pointable.id], pos);
				//console.log(dist);
				if (dist < this._gap){
					if (this._times[pointable.id] == 0){
						this._times[pointable.id] = (new Date()).getTime();
					}else{
						var h = ((new Date()).getTime() - this._times[pointable.id]);
						if ( h > GesturePushHolding.holdInterval){
							this._detected = true;
							this.reset();
						}
						//console.log("RET");
						return {time: h, x: pos[0], y:pos[1], detected: this._detected, pointerid: pointable.id};
					}
				}else{
					this._lastpos[pointable.id] = null;
					this._times[pointable.id] = 0;
				}
			}
		}
		
		//console.log("RET NULL");
		return null;
	}
	
	this.reset = function(){
		this._lastpos = [];
		this._times = [];
	}
	
	this.isDetected = function(){
		return this._detected;
	}
}