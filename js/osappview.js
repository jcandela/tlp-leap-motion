
function OSAppView(def){


	//@Override
	this.init = function(){
		OSAppView.prototype.init.call(this, {size: 50, icon: def.icon}); // Calls super method				
		
		this.addAction( new ContextualAction("", "Open", function(){
				OSGui.executeCmd(def.cmd, def.params);
		}, true));
	}
			
	
	this.init();
	
};

OSAppView.prototype = new Interactive3DView;
