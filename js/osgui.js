var OS_HOME_SCENE = "OS_HOME_SCENE";
var OS_APPS_SCENE = "OS_APPS_SCENE";
var OS_EXPLORER_SCENE = "OS_EXPLORER_SCENE";


var OS_CMD_LINKURL 		= "OS_CMD_LINKURL";
var OS_CMD_EXPLORER 	= "OS_CMD_EXPLORER";
var OS_CMD_EXPLORER_CD 	= "OS_CMD_EXPLORER_CD";
var OS_CMD_FILE_OPEN 	= "OS_CMD_FILE_OPEN";
var OS_CMD_FILE_DELETE 	= "OS_CMD_FILE_DELETE";
var OS_CMD_FILE_SHARE 	= "OS_CMD_FILE_SHARE";
var OS_CMD_FILE_COPY	= "OS_CMD_FILE_COPY";
var OS_CMD_FILE_PASTE 	= "OS_CMD_FILE_PASTE";
var OS_CMD_EXECUTE  	= "OS_CMD_EXECUTE"; 


var OS_FILEEXT_XLS = "xls";
var OS_FILEEXT_TXT = "txt";
var OS_FILEEXT_DOC = "doc";


var OSGui = {};


OSGui.init = function(){

	TLPLeap.init();
	TLP3DGui.init();
	
	OSGui.loadFakeData();
	
	// Home Scene -----------------------
	var home = new TLP3DScene(OS_HOME_SCENE);
	home.addView( new Interactive3DView({
		actions: [
			new ContextualAction("", "Go to Apps", function(){
				TLP3DGui.sceneManager.select(OS_APPS_SCENE);
			}, true)
		],
		icon : "res/apps.png",
		position: {x : 0, y: 150},
		size: 30
	}));
	//--------------
	
	
	
	
	// Apps scene ----------------------
	var sv = new Scroll3DView({ orientation : ScrollOrientation.HORIZONTAL });
	for (var i = 0; i < OSGui.data.apps.length; i++)
		sv.addView(new OSAppView(OSGui.data.apps[i]));

	var apps = new TLP3DScene(OS_APPS_SCENE);
	apps.addView(sv);
	apps.addView(new Interactive3DView({
		actions: [
			new ContextualAction("", "Go Home", function(){
				TLP3DGui.sceneManager.select(OS_HOME_SCENE);
			}, true)
		],
		icon: "res/home.png",
		position: {x : -150, y: 150},
		size: 20
	}));


	//----------------
	
	
	// Explorer scene -------------------
	var sv2 = new Scroll3DView({ orientation : ScrollOrientation.HORIZONTAL, lineSize : {w:140, h:40} });
	OSGui.explorer.content = sv2;
	
	var explorer = new TLP3DScene(OS_EXPLORER_SCENE);
	explorer.addView(sv2);
	explorer.addView(new Interactive3DView({
		actions: [
			new ContextualAction("", "Go Home", function(){
				TLP3DGui.sceneManager.select(OS_HOME_SCENE);
			}, true)
		],
		icon: "res/home.png",
		position: {x : -150, y: 150},
		size: 20
	}));
	explorer.addView(new Interactive3DView({
		actions: [
			new ContextualAction("", "Go Back", function(){
				OSGui.explorer.goBack();
			}, true)
		],
		icon: "res/back.png",
		position: {x : 150, y: 150},
		size: 20
	}));
	//----------------
	
	
	TLP3DGui.sceneManager.add(home);
	TLP3DGui.sceneManager.add(apps);
	TLP3DGui.sceneManager.add(explorer);
	
	TLP3DGui.sceneManager.select(OS_HOME_SCENE, false);
	
	
}


OSGui.loadFakeData = function(){
	OSGui.data = {};

	OSGui.data.apps = [
		{ icon:"res/facebook.png"	, cmd: OS_CMD_LINKURL, params: "http://facebook.com" },
		{ icon:"res/pinterest.png"	, cmd: OS_CMD_LINKURL, params: "http://pinterest.com" },
		{ icon:"res/twitter.jpg"	, cmd: OS_CMD_LINKURL, params: "http://twitter.com" },
		{ icon:"res/google.png"		, cmd: OS_CMD_LINKURL, params: "http://google.com" },
		{ icon:"res/deviantart.png"	, cmd: OS_CMD_LINKURL, params: "http://deviantart.com"},
		{ icon:"res/youtube.png"	, cmd: OS_CMD_LINKURL, params: "http://youtube.com"},
		{ icon:"res/flickr.png"		, cmd: OS_CMD_LINKURL, params: "http://flickr.com" },	
		{ icon:"res/explorer.png"	, cmd: OS_CMD_EXPLORER, params: "/" }				
	]
	
	OSGui.data.files = {
		"One folder" : { type: OS_FILETYPE_FOLDER, content:{} },
		"Documents" : { type: OS_FILETYPE_FOLDER, content:{
			"Folder 21" : { type: OS_FILETYPE_FOLDER, content:{} },
			"File 1.doc" : { type: OS_FILETYPE_FILE, id:"1t0tC-LtzMKYZM9OsxWhzY2NKS6VhEtpXliAf8QL20uw", size: 856},
			"File 2.doc" : { type: OS_FILETYPE_FILE, id:"1t0tC-LtzMKYZM9OsxWhzY2NKS6VhEtpXliAf8QL20uw", size:856 }	
		}},
		"Other stuff" : { type: OS_FILETYPE_FOLDER , content:{
			"File.xls" : { type: OS_FILETYPE_FILE, id:"19dolBz2wm1PTtRe3AzzyE7vDj02mwkWWxGHMN0vJ6EQ", size: 345},
			"Program.exe" 	: { type: OS_FILETYPE_EXE, cmd: "", params: "", size: 136}		
		}},
		"My things" : { type: OS_FILETYPE_FOLDER , content:{}}
	};

}

OSGui.executeCmd = function(cmd, params){
	console.log("[OSGui][info] Executing command:" + cmd + ", params: " + params);
	switch(cmd){
		case OS_CMD_LINKURL:
			window.open(
			  params,
			  '_blank'
			);
			break;
		case OS_CMD_EXPLORER:
			OSGui.explorer.loadPath(params);
			TLP3DGui.sceneManager.select(OS_EXPLORER_SCENE, true);
			break;
		case OS_CMD_EXPLORER_CD:
			// TODO - Detectar /..
			OSGui.explorer.path += params;
			OSGui.explorer.loadPath(OSGui.explorer.path);
			break;
		case OS_CMD_FILE_SHARE:
			alert("Not implemented, this is just an example");
			break;
		case OS_CMD_FILE_COPY:
			alert("Not implemented, this is just an example");
			break;
		case OS_CMD_FILE_PASTE:
			alert("Not implemented, this is just an example");
			break;
		case OS_CMD_FILE_DELETE:
			var files = OSGui.explorer.getCurrentPathFiles();
			delete files[params];
			OSGui.explorer.reloadPath(); 
			break;
		case OS_CMD_FILE_OPEN:
			console.log("Opening file "  +params.name + ", "+ params.id);

			var ext = params.name.split('.');
			ext = ext[ext.length-1];
			var filetype = "";			
			switch(ext.toLowerCase()){
				case OS_FILEEXT_XLS:
					filetype = "spreadsheets";
					break;
				case OS_FILEEXT_TXT:
				case OS_FILEEXT_DOC:
					filetype = "document";
					break;
				default:
					break;	
			}
			
			var url = "https://docs.google.com/" +filetype+ "/d/" + params.id;
			window.open(
			  url,
			  '_blank'
			);
			break;
		default:
			break;
	}
}

OSGui.explorer = {};

OSGui.explorer.path = "";

OSGui.explorer.goBack = function(){
	
	var paths = OSGui.explorer.path.split("/");
	
	if (paths.length == 1){
		TLP3DGui.sceneManager.select(OS_APPS_SCENE, true);
	}else{
		OSGui.explorer.path = "";
		for (var i = 0 ; i < paths.length - 2; i++)
			OSGui.explorer.path += paths[i] + "/";
			
		OSGui.explorer.loadPath(OSGui.explorer.path);
	}
}

OSGui.explorer.reloadPath = function(){
	OSGui.explorer.loadPath(OSGui.explorer.path);
}

OSGui.explorer.loadPath = function(path){
	if (path.charAt(0) == '/')
		path = path.slice(1, path.length);
		
	OSGui.explorer.path = path;
	OSGui.explorer.content.clearViews();
	
	var files = OSGui.explorer.getCurrentPathFiles();

	for(var f in files) 
		OSGui.explorer.content.addView(new OSFileView(f, files[f]));
			
}

OSGui.explorer.getCurrentPathFiles = function(){
	var files = {};
	var paths = OSGui.explorer.path.split("/");
	files = OSGui.data.files;		
	console.log("[OSGui][info] Opening path in explorer ...depth size: " + paths.length);		
	if (paths.length > 1){ //Not Root 
		files = files[paths[0]].content;
		for (var i = 1; i < paths.length - 1; i++){
			console.log("OPENING: "  + paths[i]);
			files = files[paths[i]].content;
		}
	}
	
	return files;
}

