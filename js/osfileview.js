
var OS_FILETYPE_FOLDER 	= "OS_FILETYPE_FOLDER";
var OS_FILETYPE_FILE 	= "OS_FILETYPE_FILE";
var OS_FILETYPE_EXE 	= "OS_FILETYPE_EXE";

function OSFileView(name, def){

	//@Override
	this.init = function(){
		var fileicon = "";
		switch(def.type){
			case OS_FILETYPE_FOLDER:
				fileicon = "res/folder.png";
				break;
			case OS_FILETYPE_FILE:
				var ext = name.split('.');
				ext = ext[ext.length-1];
				fileicon = "res/" + ext + ".png";		
				break;
			case OS_FILETYPE_EXE:
				fileicon = "res/rar.png";
				break;
		}
		
		// Calls super method
		OSAppView.prototype.init.call(this, {size: 50, icon: fileicon, actions: [
			new ContextualAction("res/open.png", "Open", function(){
				switch(def.type){
					case OS_FILETYPE_FOLDER:
						OSGui.executeCmd(OS_CMD_EXPLORER_CD, name + "/");
						break;
					case OS_FILETYPE_FILE:
						OSGui.executeCmd(OS_CMD_FILE_OPEN, {name : name, id: def.id} );
						break;
					case OS_FILETYPE_EXE:
						OSGui.executeCmd(OS_CMD_EXECUTE, name + "/");
						break;
				}
			}, true),
			new ContextualAction("res/copy.png", "Copy", function(){
				OSGui.executeCmd(OS_CMD_FILE_COPY, name);
			}, false),
			new ContextualAction("res/paste.png", "Paste", function(){
				OSGui.executeCmd(OS_CMD_FILE_PASTE, name);
			}, false),
			new ContextualAction("res/delete.png", "Remove", function(){
				OSGui.executeCmd(OS_CMD_FILE_DELETE, name);
			}, false),
			new ContextualAction("res/send.png", "Send", function(){
				OSGui.executeCmd(OS_CMD_FILE_SHARE, name);
			}, false)
		]}); 
		
		
		// Label
		var txt = makeTextSprite(name, {
			fontsize : 28,
			color: "#333",
			textAlign: "left"
		});
		txt.position.x = 50;
		txt.position.z = -10;
		this.mesh.add(txt);
		
		// Items or size
		if (def.type == OS_FILETYPE_FOLDER){
			var size = 0;
			for (var i in def.content)
				size++;
				
			var txt = makeTextSprite(size + " ITEMS", {
				fontsize : 20,
				color: "#888",
				textAlign: "left"
			});
			txt.position.x = 50;
			txt.position.z = 10;
			this.mesh.add(txt);
		}else{
			var txt = makeTextSprite(def.size + " Kbs", {
				fontsize : 24,
				color: "#888",
				textAlign: "left"
			});
			txt.position.x = 50;
			txt.position.z = 10;
			this.mesh.add(txt);
		}
		
		this.mesh.scale.y = 0.01;
			
	
	}
	
	this.init();
}

OSFileView.prototype = new Interactive3DView;